Flipflop est un jeu original développé pour apporter une expérience amusante !


## Configuration
- Node 16.16.0
- php 8.1.0 (cli)
- composer 2.3.9
- Wampserver64 (wamp configuration disponible dans le repository)
## Setup
```
git clone https://gitlab.com/Hugo2711/jwtauthflipflop.git
cd folder
composer install
//Generate key for jwtToken
php bin/console lexik:jwt:generate-keypair
//Configure database in .env
DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7"
//create database (don't forget to start Wampserver)
php bin/console doctrine:database:create
//To start the server
symfony serve
```
