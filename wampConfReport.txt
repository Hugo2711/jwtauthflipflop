1660559551
------ Wampserver configuration report
lundi 15 août 2022 - 10:32
- Windows NT 10.0 build 19043 (Windows 10)
- Windows Charset: Windows-1252
- Wampserver version 3.2.6 - 64bit
- Wampserver install version 3.2.6
- Install directory: c:/wamp64
- Default browser: C:/Program Files (x86)/Internet Explorer/iexplore.exe
- Default text editor: C:/Windows/system32/notepad.exe
- Default log viewer: C:/Windows/system32/notepad.exe
- Apache 2.4.51 - Port 80
- Additional Apache listening ports:
- PHP 7.4.26
MySQL versions seen by refresh listDir:
5.7.36
MariaDB versions seen by refresh listDir:
10.6.5
PHP versions seen by refresh listDir:
5.6.40 - 7.4.26 - 8.0.13 - 8.1.0
Apache versions seen by refresh listDir:
2.4.51
- MySQL 5.7.36 Port 3306
MySQL basedir = c:/wamp64/bin/mysql/mysql5.7.36
MySQL datadir = c:/wamp64/bin/mysql/mysql5.7.36/data
- MariaDB 10.6.5 Port 3307
MariaDB basedir = c:/wamp64/bin/mariadb/mariadb10.6.5
MariaDB datadir = c:/wamp64/bin/mariadb/mariadb10.6.5/data
- PHP 5.6.40 for CLI (Internal Wampserver PHP scripts)
------ Wampserver configuration ------
---------- Section [options]
AliasSubmenu = off                 NotCheckVirtualHost = off
NotCheckDuplicate = off            VirtualHostSubMenu = on
HomepageAtStartup = off            VhostAllLocalIp = off
SupportMySQL = on                  SupportMariaDB = on
ShowphmyadMenu = on                ShowadminerMenu = on
NotVerifyPATH = off                HostsLinesLimit = 5000
NotVerifyHosts = off               NotVerifyTLD = off
AutoCleanLogs = on                 AutoCleanLogsMax = 1000
AutoCleanLogsMin = 50              AutoCleanTmp = on
AutoCleanTmpMax = 1000             BackupHosts = off
ShowWWWdirMenu = off               MysqlMariaChangePrompt = off
LinksOnProjectsHomePage = off      LocalTest = off
---------- Section [apacheoptions]
apacheUseOtherPort = off           apachePortUsed = 80
apacheCompareVersion = off         apacheRestoreFiles = off
apacheGracefulRestart = on         apachePhpCurlDll = off
---------- Section [mysqloptions]
mysqlPortUsed = 3306               mysqlUseOtherPort = off
mysqlUseConsolePrompt = off        mysqlConsolePrompt = \U-MySQL\v-['\d']>
mysqlPortOptionsMenu = on
---------- Section [mariadboptions]
mariaPortUsed = 3307               mariaUseOtherPort = on
mariadbUseConsolePrompt = off      mariadbConsolePrompt = \U-\v-['\d']>
mariadbPortOptionsMenu = on
---------------------------------------------
------ C:/Windows/system32/drivers/etc/hosts file contents ------
------ Limited to the first 30 lines ------
#
127.0.0.1 localhost
::1 localhost
----------------------------------------------
-- c:/wamp64/bin/apache/apache2.4.51/conf/extra/httpd-vhosts.conf file contents --
------ Limited to the first 40 lines ------
# Virtual Hosts
#
<VirtualHost *:80>
  ServerName localhost
  ServerAlias localhost
  DocumentRoot "${INSTALL_DIR}/www"
  <Directory "${INSTALL_DIR}/www/">
    Options +Indexes +Includes +FollowSymLinks +MultiViews
    AllowOverride All
    Require local
  </Directory>
</VirtualHost>
----------------------------------------------
Warning: It seems that a PHP installation is declared in the environment variable PATH
C:\Program Files (x86)\php\php.exe
Warning: There is Wampserver path (c:/wamp64)
into Windows PATH environnement variable: (C:\wamp64\bin\php\php8.1.0)

Wampserver does not use, modify or require the PATH environment variable.
Using a PATH on Wampserver or PHP version
may be detrimental to the proper functioning of Wampserver.
-- PHP Configuration values
 allow_url_fopen = On                    allow_url_include = Off
 auto_append_file =                      auto_globals_jit = On
 auto_prepend_file =                     date.timezone = UTC
 default_charset = UTF-8                 default_mimetype = text/html
 disable_classes =                       disable_functions =
 display_errors = On                     display_startup_errors = On
 enable_dl = Off                         error_reporting = E_ALL
 expose_php = On                         file_uploads = On
 ignore_repeated_errors = Off            ignore_repeated_source = Off
 implicit_flush = Off                    log_errors = On
 max_execution_time = 120                max_input_time = 60
 max_input_vars = 2500                   memory_limit = 128M
 output_buffering = 4096                 post_max_size = 8M
 register_argc_argv = Off                report_memleaks = On
 request_order = GP                      session.save_path = c:/wamp64/tmp
 short_open_tag = Off                    upload_max_filesize = 2M
 upload_tmp_dir = c:/wamp64/tmp          variables_order = GPCS
 xdebug.log_level = 7                    xdebug.mode = develop
 xdebug.show_local_vars = 0              zend.enable_gc = On
 zlib.output_compression = Off
--------------------------------------------------
--------- wampmanager.ini (Last 4 lines) --------
[Options]
AeTrayMode=64 Bits
AeTrayOldMode=64bit
AeTrayVersion=3.2.4.2
--------------------------------------------------
State of services:
 The service 'wampapache64' is started
 Service Session : LocalSystem
 The service 'wampmysqld64' is started
 Service Session : LocalSystem
 The service 'wampmariadb64' is started
 Service Session : LocalSystem
	all services are started - it is OK
	all services BINARY_PATH_NAME are OK
--------------------------------------------------
*** Checking the DNS search order ***
Values of registry keys for
HKLM\SYSTEM\CurrentControlSet\Services\Tcpip\ServiceProvider
are in correct order
--------------------------------------------------
Wampmanager (Aestan Tray Menu) 3.2.4.2 - 64bit
Compiler Visual C++ versions used:
PHP 5.6.40 Compiler => MSVC11 (Visual C++ 2012) - Architecture => x64
PHP 7.4.26 Compiler => Visual C++ 2017 - Architecture => x64
PHP 8.0.13 Compiler => Visual C++ 2019 - Architecture => x64
PHP 8.1.0 Compiler => Visual C++ 2019 - Architecture => x64
MySQL Ver 5.7.36 for Win64 on x86_64 (MySQL Community Server (GPL))
MariaDB Ver 10.6.5-MariaDB for Win64 on AMD64 (mariadb.org binary distribution)
Apache 2.4.51 Apache Lounge VS16 Server built:   Oct  7 2021 16:27:02
	Architecture:   64-bit
--------------------------------------------------
-- Apache loaded modules
- Core:
 core_module       win32_module      mpm_winnt_module
 http_module       so_module
- Shared modules:
 access_compat_module    actions_module          alias_module
 allowmethods_module     asis_module             auth_basic_module
 auth_digest_module      authn_core_module       authn_file_module
 authz_core_module       authz_groupfile_module  authz_host_module
 authz_user_module       autoindex_module        cache_module
 cache_disk_module       cgi_module              dir_module
 env_module              file_cache_module       include_module
 isapi_module            log_config_module       mime_module
 negotiation_module      rewrite_module          setenvif_module
 userdir_module          vhost_alias_module      php7_module
--------------------------------------------------
Apache includes
Included configuration files:
  (*) C:/wamp64/bin/apache/apache2.4.51/conf/httpd.conf
    (535) C:/wamp64/bin/apache/apache2.4.51/conf/extra/httpd-autoindex.conf
    (547) C:/wamp64/bin/apache/apache2.4.51/conf/extra/httpd-vhosts.conf
    (575) C:/wamp64/alias/adminer.conf
    (575) C:/wamp64/alias/phpmyadmin.conf
    (575) C:/wamp64/alias/phpmyadmin4.9.7.conf
    (575) C:/wamp64/alias/phpsysinfo.conf
--------------------------------------------------
         Apache variables (Define)
   - With command httpd.exe -t -D DUMP_RUN_CFG
APACHE24 = Apache2.4
VERSION_APACHE = 2.4.51
INSTALL_DIR = c:/wamp64
APACHE_DIR = c:/wamp64/bin/apache/apache2.4.51
SRVROOT = c:/wamp64/bin/apache/apache2.4.51
--------------------------------------------------
VirtualHost configuration:
Virtual Host: localhost
--------------------------------------------------
-- PHP Loaded Extensions
 With function get_loaded_extensions()
 bcmath        bz2           calendar      com_dotnet    Core          ctype
 curl          date          dom           exif          fileinfo      filter
 gd            gettext       gmp           hash          iconv         imap
 intl          json          ldap          libxml        mbstring      mysqli
 mysqlnd       openssl       pcre          PDO           pdo_mysql     pdo_sqlite
 Phar          readline      Reflection    session       SimpleXML     soap
 sockets       SPL           sqlite3       standard      tokenizer     xdebug
 xml           xmlreader     xmlrpc        xmlwriter     xsl           Zend OPcache
 zip           zlib
--------------------------------------------------
***** Check symbolic links *****
All symbolic links are OK
--------------------------------------------------
***** Test which uses port 80 *****
===== Tested by command netstat filtered on port 80 =====
Test for TCP
Your port 80 is used by a processus with PID = 7564
The processus of PID 7564 is 'httpd.exe' Session: Services
The service of PID 7564 for 'httpd.exe' is 'wampapache64'
This service is from Wampserver - It is correct
Test for TCPv6
Your port 80 is used by a processus with PID = 7564
The processus of PID 7564 is 'httpd.exe' Session: Services
The service of PID 7564 for 'httpd.exe' is 'wampapache64'
This service is from Wampserver - It is correct
===== Tested by attempting to open a socket on port 80 =====
Your port 80 is actually used by :
Server: Apache/2.4.51 (Win64) PHP/7.4.26
--------------------------------------------------
***** Test which uses port 3306 *****
===== Tested by command netstat filtered on port 3306 =====
Test for TCP
Your port 3306 is used by a processus with PID = 7236
The processus of PID 7236 is 'mysqld.exe' Session: Services
The service of PID 7236 for 'mysqld.exe' is 'wampmysqld64'
This service is from Wampserver - It is correct
Test for TCPv6
Your port 3306 is used by a processus with PID = 7236
The processus of PID 7236 is 'mysqld.exe' Session: Services
The service of PID 7236 for 'mysqld.exe' is 'wampmysqld64'
This service is from Wampserver - It is correct
--------------------------------------------------
***** Test which uses port 3307 *****
===== Tested by command netstat filtered on port 3307 =====
Test for TCP
Your port 3307 is used by a processus with PID = 8760
The processus of PID 8760 is 'mysqld.exe' Session: Services
The service of PID 8760 for 'mysqld.exe' is 'wampmariadb64'
This service is from Wampserver - It is correct
Test for TCPv6
Your port 3307 is used by a processus with PID = 8760
The processus of PID 8760 is 'mysqld.exe' Session: Services
The service of PID 8760 for 'mysqld.exe' is 'wampmariadb64'
This service is from Wampserver - It is correct
--------------------------------------------------
-------- Apache error log (Last 30 lines) --------
--- File cleaned up by Wampserver ---
--- on 2022-08-14 13:10
[Sun Aug 14 15:10:30.629818 2022] [mpm_winnt:notice] [pid 1548:tid 740] AH00455: Apache/2.4.51 (Win64) PHP/7.4.26 configured -- resuming normal operations
[Sun Aug 14 15:10:30.629818 2022] [mpm_winnt:notice] [pid 1548:tid 740] AH00456: Apache Lounge VS16 Server built: Oct  7 2021 16:27:02
[Sun Aug 14 15:10:30.629818 2022] [core:notice] [pid 1548:tid 740] AH00094: Command line: 'c:\\wamp64\\bin\\apache\\apache2.4.51\\bin\\httpd.exe -d C:/wamp64/bin/apache/apache2.4.51'
[Sun Aug 14 15:10:30.631819 2022] [mpm_winnt:notice] [pid 1548:tid 740] AH00418: Parent: Created child process 11860
[Sun Aug 14 15:10:30.955733 2022] [mpm_winnt:notice] [pid 11860:tid 668] AH00354: Child: Starting 64 worker threads.
[Sun Aug 14 15:13:08.560580 2022] [mpm_winnt:notice] [pid 1548:tid 740] AH00422: Parent: Received shutdown signal -- Shutting down the server.
[Sun Aug 14 15:13:10.578035 2022] [mpm_winnt:notice] [pid 11860:tid 668] AH00364: Child: All worker threads have exited.
[Sun Aug 14 15:13:10.600040 2022] [mpm_winnt:notice] [pid 1548:tid 740] AH00430: Parent: Child process 11860 exited successfully.
[Sun Aug 14 15:13:13.288648 2022] [mpm_winnt:notice] [pid 3712:tid 740] AH00455: Apache/2.4.51 (Win64) PHP/7.4.26 configured -- resuming normal operations
[Sun Aug 14 15:13:13.288648 2022] [mpm_winnt:notice] [pid 3712:tid 740] AH00456: Apache Lounge VS16 Server built: Oct  7 2021 16:27:02
[Sun Aug 14 15:13:13.288648 2022] [core:notice] [pid 3712:tid 740] AH00094: Command line: 'c:\\wamp64\\bin\\apache\\apache2.4.51\\bin\\httpd.exe -d C:/wamp64/bin/apache/apache2.4.51'
[Sun Aug 14 15:13:13.291648 2022] [mpm_winnt:notice] [pid 3712:tid 740] AH00418: Parent: Created child process 10404
[Sun Aug 14 15:13:13.615722 2022] [mpm_winnt:notice] [pid 10404:tid 792] AH00354: Child: Starting 64 worker threads.
[Sun Aug 14 19:36:31.377780 2022] [mpm_winnt:notice] [pid 3712:tid 740] AH00422: Parent: Received shutdown signal -- Shutting down the server.
[Sun Aug 14 19:36:33.549801 2022] [mpm_winnt:notice] [pid 10404:tid 792] AH00364: Child: All worker threads have exited.
[Sun Aug 14 19:36:34.909269 2022] [mpm_winnt:notice] [pid 3712:tid 740] AH00430: Parent: Child process 10404 exited successfully.
[Sun Aug 14 19:50:03.771319 2022] [mpm_winnt:notice] [pid 7564:tid 724] AH00455: Apache/2.4.51 (Win64) PHP/7.4.26 configured -- resuming normal operations
[Sun Aug 14 19:50:03.782321 2022] [mpm_winnt:notice] [pid 7564:tid 724] AH00456: Apache Lounge VS16 Server built: Oct  7 2021 16:27:02
[Sun Aug 14 19:50:03.782321 2022] [core:notice] [pid 7564:tid 724] AH00094: Command line: 'c:\\wamp64\\bin\\apache\\apache2.4.51\\bin\\httpd.exe -d C:/wamp64/bin/apache/apache2.4.51'
[Sun Aug 14 19:50:03.784322 2022] [mpm_winnt:notice] [pid 7564:tid 724] AH00418: Parent: Created child process 7368
[Sun Aug 14 19:50:04.093392 2022] [mpm_winnt:notice] [pid 7368:tid 720] AH00354: Child: Starting 64 worker threads.
--------------------------------------------------
-------- Apache access log (Last 30 lines) --------
::1 - - [15/Aug/2022:05:49:35 +0200] "GET /phpmyadmin/themes/pmahomme/css/theme.css?v=5.1.1&nocache=3717237362ltr&server=1 HTTP/1.1" 200 214799
::1 - - [15/Aug/2022:05:49:35 +0200] "GET /phpmyadmin/index.php?route=/recent-table&ajax_request=1&recent_table=1&no_debug=true&_nocache=1660535375880967930&token=596c756f44395259363f404e605d5951 HTTP/1.1" 200 1678
::1 - - [15/Aug/2022:05:49:35 +0200] "POST /phpmyadmin/index.php?route=/navigation&ajax_request=1 HTTP/1.1" 200 2635
::1 - - [15/Aug/2022:05:49:35 +0200] "POST /phpmyadmin/index.php?route=/config/get HTTP/1.1" 200 1623
::1 - - [15/Aug/2022:05:49:35 +0200] "POST /phpmyadmin/index.php?route=/config/get HTTP/1.1" 200 1715
::1 - - [15/Aug/2022:05:49:36 +0200] "GET /phpmyadmin/index.php?route=/navigation&ajax_request=1&server=1&aPath=cm9vdA%3D%3D.and0YXV0aGZscA%3D%3D&vPath=cm9vdA%3D%3D.and0YXV0aGZscA%3D%3D&pos=0&searchClause=&searchClause2=&_nocache=166053537600380159&token=596c756f44395259363f404e605d5951 HTTP/1.1" 200 2144
::1 - - [15/Aug/2022:05:49:36 +0200] "POST /phpmyadmin/index.php?route=/config/set HTTP/1.1" 200 1616
::1 - - [15/Aug/2022:05:49:37 +0200] "GET /phpmyadmin/index.php?route=/sql&server=1&db=jwtauthflp&table=user&pos=0&ajax_request=true&ajax_page_request=true&_nocache=166053537743045021&token=596c756f44395259363f404e605d5951 HTTP/1.1" 200 8688
::1 - - [15/Aug/2022:05:49:37 +0200] "GET /phpmyadmin/index.php?route=/recent-table&ajax_request=1&recent_table=1&no_debug=true&_nocache=1660535377664544913&token=596c756f44395259363f404e605d5951 HTTP/1.1" 200 1678
::1 - - [15/Aug/2022:05:49:47 +0200] "POST /phpmyadmin/index.php?route=/table/replace HTTP/1.1" 200 2185
::1 - - [15/Aug/2022:05:50:47 +0200] "-" 408 -
::1 - - [15/Aug/2022:06:13:32 +0200] "POST /phpmyadmin/index.php?route=/ HTTP/1.1" 200 1679
::1 - - [15/Aug/2022:06:13:47 +0200] "POST /phpmyadmin/index.php?route=/ HTTP/1.1" 200 1679
::1 - - [15/Aug/2022:06:13:48 +0200] "POST /phpmyadmin/index.php?route=/ HTTP/1.1" 200 1679
::1 - - [15/Aug/2022:06:13:49 +0200] "POST /phpmyadmin/index.php?route=/ HTTP/1.1" 200 2426
::1 - - [15/Aug/2022:06:14:47 +0200] "-" 408 -
::1 - - [15/Aug/2022:06:55:36 +0200] "POST /phpmyadmin/index.php?route=/ HTTP/1.1" 200 1692
::1 - - [15/Aug/2022:06:55:36 +0200] "GET /phpmyadmin/index.php?route=/recent-table&ajax_request=1&recent_table=1&no_debug=true&_nocache=1660539336994778818&token=483f5242357a455532336a3a49343758 HTTP/1.1" 200 1644
::1 - - [15/Aug/2022:06:55:40 +0200] "POST /phpmyadmin/index.php?route=/table/replace HTTP/1.1" 200 2187
::1 - - [15/Aug/2022:06:55:46 +0200] "POST /phpmyadmin/index.php?route=/table/replace HTTP/1.1" 200 2182
::1 - - [15/Aug/2022:06:55:51 +0200] "POST /phpmyadmin/index.php?route=/table/replace HTTP/1.1" 200 2187
::1 - - [15/Aug/2022:06:55:55 +0200] "POST /phpmyadmin/index.php?route=/table/replace HTTP/1.1" 200 2185
::1 - - [15/Aug/2022:06:56:51 +0200] "-" 408 -
::1 - - [15/Aug/2022:07:19:31 +0200] "POST /phpmyadmin/index.php?route=/ HTTP/1.1" 200 1679
::1 - - [15/Aug/2022:07:20:32 +0200] "-" 408 -
::1 - - [15/Aug/2022:07:24:22 +0200] "POST /phpmyadmin/index.php?route=/ HTTP/1.1" 200 1679
::1 - - [15/Aug/2022:07:24:23 +0200] "POST /phpmyadmin/index.php?route=/ HTTP/1.1" 200 1679
::1 - - [15/Aug/2022:07:24:24 +0200] "POST /phpmyadmin/index.php?route=/ HTTP/1.1" 200 2426
::1 - - [15/Aug/2022:07:25:22 +0200] "-" 408 -
127.0.0.1 - - [15/Aug/2022:12:32:30 +0200] "GET / HTTP/1.1" 200 6341
--------------------------------------------------
-------- PHP error log (Last 30 lines) --------
[14-Aug-2022 19:39:08 UTC] [critical] Error thrown while running command "make:migration". Message: "No mapping information to process"
[14-Aug-2022 19:40:15 UTC] [critical] Error thrown while running command "make:migration". Message: "No mapping information to process"
[14-Aug-2022 19:40:15 UTC] 2022-08-14T19:40:15+00:00 [info] User Deprecated: The "Symfony\Bridge\Doctrine\Logger\DbalLogger" class implements "Doctrine\DBAL\Logging\SQLLogger" that is deprecated Use {@see \Doctrine\DBAL\Logging\Middleware} or implement {@see \Doctrine\DBAL\Driver\Middleware} instead.
[14-Aug-2022 19:40:40 UTC] [critical] Error thrown while running command "make:migration". Message: "No mapping information to process"
[14-Aug-2022 19:40:40 UTC] 2022-08-14T19:40:40+00:00 [info] User Deprecated: The "Symfony\Bridge\Doctrine\Logger\DbalLogger" class implements "Doctrine\DBAL\Logging\SQLLogger" that is deprecated Use {@see \Doctrine\DBAL\Logging\Middleware} or implement {@see \Doctrine\DBAL\Driver\Middleware} instead.
[14-Aug-2022 21:45:41 UTC] [critical] Error thrown while running command ""make:auth"". Message: "There are no commands defined in the "make" namespace.
You may be looking for a command provided by the "MakerBundle" which is currently not installed. Try running "composer require symfony/maker-bundle --dev"."
[14-Aug-2022 21:45:48 UTC] [critical] Error thrown while running command ""make:auth"". Message: "There are no commands defined in the "make" namespace.
You may be looking for a command provided by the "MakerBundle" which is currently not installed. Try running "composer require symfony/maker-bundle --dev"."
[14-Aug-2022 21:50:06 UTC] [critical] Error thrown while running command ""doctrine:data:create"". Message: "There are no commands defined in the "doctrine:data" namespace.
You may be looking for a command provided by the "Doctrine ORM" which is currently not installed. Try running "composer require symfony/orm-pack"."
[14-Aug-2022 21:51:07 UTC] 2022-08-14T21:51:07+00:00 [info] User Deprecated: The "Symfony\Bridge\Doctrine\Logger\DbalLogger" class implements "Doctrine\DBAL\Logging\SQLLogger" that is deprecated Use {@see \Doctrine\DBAL\Logging\Middleware} or implement {@see \Doctrine\DBAL\Driver\Middleware} instead.
[14-Aug-2022 21:51:15 UTC] [critical] Error thrown while running command ""doctrine:data:create"". Message: "An exception occurred in the driver: could not find driver"
[14-Aug-2022 21:52:00 UTC] 2022-08-14T21:52:00+00:00 [info] User Deprecated: The "Symfony\Bridge\Doctrine\Logger\DbalLogger" class implements "Doctrine\DBAL\Logging\SQLLogger" that is deprecated Use {@see \Doctrine\DBAL\Logging\Middleware} or implement {@see \Doctrine\DBAL\Driver\Middleware} instead.
[14-Aug-2022 21:52:24 UTC] 2022-08-14T21:52:24+00:00 [info] User Deprecated: The "Symfony\Bridge\Doctrine\Logger\DbalLogger" class implements "Doctrine\DBAL\Logging\SQLLogger" that is deprecated Use {@see \Doctrine\DBAL\Logging\Middleware} or implement {@see \Doctrine\DBAL\Driver\Middleware} instead.
[14-Aug-2022 21:53:01 UTC] 2022-08-14T21:53:01+00:00 [info] User Deprecated: The "Symfony\Bridge\Doctrine\Logger\DbalLogger" class implements "Doctrine\DBAL\Logging\SQLLogger" that is deprecated Use {@see \Doctrine\DBAL\Logging\Middleware} or implement {@see \Doctrine\DBAL\Driver\Middleware} instead.
[14-Aug-2022 21:55:15 UTC] 2022-08-14T21:55:15+00:00 [info] User Deprecated: The "Symfony\Bridge\Doctrine\Logger\DbalLogger" class implements "Doctrine\DBAL\Logging\SQLLogger" that is deprecated Use {@see \Doctrine\DBAL\Logging\Middleware} or implement {@see \Doctrine\DBAL\Driver\Middleware} instead.
[14-Aug-2022 21:55:33 UTC] 2022-08-14T21:55:33+00:00 [info] User Deprecated: The "Symfony\Bridge\Doctrine\Logger\DbalLogger" class implements "Doctrine\DBAL\Logging\SQLLogger" that is deprecated Use {@see \Doctrine\DBAL\Logging\Middleware} or implement {@see \Doctrine\DBAL\Driver\Middleware} instead.
[14-Aug-2022 21:55:45 UTC] 2022-08-14T21:55:45+00:00 [info] Deprecated: addslashes(): Passing null to parameter #1 ($string) of type string is deprecated
--------------------------------------------------
-------- MySQL error log (Last 30 lines) --------
--- File cleaned up by Wampserver ---
--- on 2022-08-14 13:10
--------------------------------------------------
-------- MariaDB error log (Last 30 lines) --------
2022-08-14 15:13:14 0 [Note] InnoDB: File './ibtmp1' size is now 12 MB.
2022-08-14 15:13:14 0 [Note] InnoDB: 10.6.5 started; log sequence number 42760; transaction id 14
2022-08-14 15:13:14 0 [Note] Plugin 'FEEDBACK' is disabled.
2022-08-14 15:13:15 0 [Note] Server socket created on IP: '::'.
2022-08-14 15:13:15 0 [Note] Server socket created on IP: '0.0.0.0'.
2022-08-14 15:13:15 0 [Note] c:\wamp64\bin\mariadb\mariadb10.6.5\bin\mysqld.exe: ready for connections.
Version: '10.6.5-MariaDB'  socket: ''  port: 3307  mariadb.org binary distribution
2022-08-14 19:36:32 0 [Note] Windows service "wampmariadb64":  received SERVICE_CONTROL_SHUTDOWN
2022-08-14 19:36:33 0 [Note] c:\wamp64\bin\mariadb\mariadb10.6.5\bin\mysqld.exe (unknown): Arrêt normal du serveur
2022-08-14 19:36:36 0 [Note] InnoDB: FTS optimize thread exiting.
2022-08-14 19:36:50 0 [Note] InnoDB: Starting shutdown...
2022-08-14 19:36:54 0 [Note] InnoDB: Removed temporary tablespace data file: "./ibtmp1"
2022-08-14 19:36:54 0 [Note] InnoDB: Shutdown completed; log sequence number 42772; transaction id 15
2022-08-14 19:36:55 0 [Note] c:\wamp64\bin\mariadb\mariadb10.6.5\bin\mysqld.exe: Arrêt du serveur terminé
2022-08-14 19:50:06 0 [Note] InnoDB: Compressed tables use zlib 1.2.11
2022-08-14 19:50:06 0 [Note] InnoDB: Number of pools: 1
2022-08-14 19:50:06 0 [Note] InnoDB: Using crc32 + pclmulqdq instructions
2022-08-14 19:50:06 0 [Note] InnoDB: Initializing buffer pool, total size = 1073741824, chunk size = 134217728
2022-08-14 19:50:06 0 [Note] InnoDB: Completed initialization of buffer pool
2022-08-14 19:50:07 0 [Note] InnoDB: 128 rollback segments are active.
2022-08-14 19:50:07 0 [Note] InnoDB: Creating shared tablespace for temporary tables
2022-08-14 19:50:07 0 [Note] InnoDB: Setting file './ibtmp1' size to 12 MB. Physically writing the file full; Please wait ...
2022-08-14 19:50:07 0 [Note] InnoDB: File './ibtmp1' size is now 12 MB.
2022-08-14 19:50:07 0 [Note] InnoDB: 10.6.5 started; log sequence number 42772; transaction id 14
2022-08-14 19:50:07 0 [Note] Plugin 'FEEDBACK' is disabled.
2022-08-14 19:50:07 0 [Note] Server socket created on IP: '::'.
2022-08-14 19:50:07 0 [Note] Server socket created on IP: '0.0.0.0'.
2022-08-14 19:50:07 0 [Note] c:\wamp64\bin\mariadb\mariadb10.6.5\bin\mysqld.exe: ready for connections.
Version: '10.6.5-MariaDB'  socket: ''  port: 3307  mariadb.org binary distribution
--------------------------------------------------